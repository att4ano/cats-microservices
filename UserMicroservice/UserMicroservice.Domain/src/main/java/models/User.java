package models;

import lombok.Builder;
import lombok.Getter;
import lombok.RequiredArgsConstructor;
import lombok.Setter;

@Setter
@Getter
@Builder
@RequiredArgsConstructor
public class User {
    private final Long id;
    private final String username;
    private final String password;
    private final String role;
    private final Long masterId;
}
