package controllers;

import contracts.MasterService;
import dto.CatDto;
import dto.MasterDto;
import kafkaDto.CreateMasterDto;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/master")
@RequiredArgsConstructor
@ComponentScan(basePackages = {"application"})
public class MasterController {
    private final MasterService masterService;

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<MasterDto>> getAllMasters() {
        return ResponseEntity.ok(masterService.checkAllMasters()
                .stream()
                .toList());
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<MasterDto> getMasterById(@PathVariable("id") long id) {
        return ResponseEntity.ok(masterService.findMasterById(id));
    }

    @GetMapping("/name/{name}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<MasterDto> getMasterByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(masterService.findMasterByName(name));
    }

    @GetMapping("/cats/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<CatDto>> getCatsOfMaster(@PathVariable("id") long id) {
        return ResponseEntity.ok(masterService.checkAllCatsOfMaster(id)
                .stream()
                .toList());
    }

    @PostMapping("/")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void addMaster(@RequestBody CreateMasterDto masterDto) {
        masterService.addMaster(masterDto.name(), masterDto.birthdayDate());
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public void deleteMaster(@PathVariable("id") Long id) {
        masterService.deleteMaster(id);
    }

}