package controllers;

import commons.ApplicationException;
import config.security.UserDetailsImpl;
import contracts.CatService;
import dto.CatDto;
import dto.FilterDto;
import endpointDto.CreateCatDto;
import endpointDto.FriendshipDto;
import lombok.RequiredArgsConstructor;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cat")
@RequiredArgsConstructor
@ComponentScan(basePackages = {"application"})
public class CatController {

    private final CatService catService;

    @GetMapping("/")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<CatDto>> getAllCats(@RequestParam(value = "color", required = false) String color,
                                                   @RequestParam(value = "breed", required = false) String breed) {
        UserDetails user = (UserDetails) SecurityContextHolder
                .getContext()
                .getAuthentication()
                .getPrincipal();

        UserDetailsImpl userDetails = (UserDetailsImpl) user;

        if (userDetails.getUser().getMasterId() == null) {
            System.out.println();
            return null;
        }

        return ResponseEntity.ok(
                catService.checkAllCats(new FilterDto(color, ((UserDetailsImpl) user).getUser().getMasterId(), breed)));
    }

    @GetMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<CatDto> getCatById(@PathVariable("id") Long id) throws ApplicationException {
        return ResponseEntity.ok(catService.findCat(id));
    }

    @GetMapping("/friends/{id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<List<CatDto>> getAllFriendsOfCat(@PathVariable("id") Long id) {
        return ResponseEntity.ok(catService.checkAllFriendsOfCat(id));
    }

    @PostMapping("/")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<String> addCat(@RequestBody CreateCatDto catDto) {
        catService.addCat(catDto.name(), catDto.birthdayDate(), catDto.breed(), catDto.color());
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/{id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<String> deleteCat(@PathVariable("id") Long id) {
        catService.deleteCat(id);
        return ResponseEntity.ok("Success");
    }

    @PostMapping("/friends/make")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<String> makeFriends(@RequestBody FriendshipDto friendshipDto) {
        catService.makeFriends(friendshipDto.firstId(), friendshipDto.secondId());
        return ResponseEntity.ok("Success");
    }

    @DeleteMapping("/friends/end")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public ResponseEntity<String> endFriendShip(@RequestBody FriendshipDto friendshipDto) {
        catService.endFriendship(friendshipDto.firstId(), friendshipDto.secondId());
        return ResponseEntity.ok("Success");
    }

    @PostMapping("/{cat_id}/{master_id}")
    @PreAuthorize("hasAuthority('ROLE_USER')")
    public void addCatToMaster(@PathVariable("cat_id") Long catId, @PathVariable("master_id") Long masterId) {
        catService.addCatToMaster(catId, masterId);
    }

}