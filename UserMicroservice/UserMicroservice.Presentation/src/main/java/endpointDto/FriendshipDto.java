package endpointDto;

public record FriendshipDto(Long firstId, Long secondId) { }