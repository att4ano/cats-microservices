package endpointDto;

public record CreateMasterDto(String name, String birthdayDate) { }
