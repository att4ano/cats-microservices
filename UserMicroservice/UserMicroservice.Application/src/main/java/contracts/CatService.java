package contracts;

import commons.ApplicationException;
import dto.CatDto;
import dto.FilterDto;

import java.util.List;

public interface CatService {

    List<CatDto> checkAllCats(FilterDto filterDto);

    List<CatDto> checkAllFriendsOfCat(Long catId);

    CatDto findCat(Long id) throws ApplicationException;

    void addCat(String name, String birthday, String breed, String color);

    void deleteCat(Long id);

    void makeFriends(Long firstCatId, Long secondCatId);

    void endFriendship(Long firstCatId, Long secondCatId);

    void addCatToMaster(Long catId, Long masterId);
}