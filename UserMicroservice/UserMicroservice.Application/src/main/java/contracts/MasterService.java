package contracts;

import dto.CatDto;
import dto.MasterDto;

import java.util.List;

public interface MasterService {

    List<MasterDto> checkAllMasters();

    MasterDto findMasterById(Long id);

    MasterDto findMasterByName(String name);

    List<CatDto> checkAllCatsOfMaster(Long masterId);

    void addMaster(String name, String birthday);

    void deleteMaster(Long id);
}