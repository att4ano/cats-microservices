package contracts;

import models.User;

import java.util.List;

public interface UserService {

    List<User> getAll();

    User getUserById(Long id);

    void addUser(String username, String password, String role);

    void deleteUser(Long id);
}
