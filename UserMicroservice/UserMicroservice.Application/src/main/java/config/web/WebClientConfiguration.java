package config.web;

import io.netty.channel.ChannelOption;
import io.netty.handler.timeout.ReadTimeoutHandler;
import io.netty.handler.timeout.WriteTimeoutHandler;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.client.reactive.ReactorClientHttpConnector;
import org.springframework.web.reactive.function.client.WebClient;
import reactor.netty.http.client.HttpClient;
import reactor.netty.tcp.TcpClient;

import java.util.concurrent.TimeUnit;

@Configuration
public class WebClientConfiguration {

    private static final String MASTER_BASE_URL = "http://localhost:8081/master";
    private static final String CAT_BASE_URL = "http://localhost:8082/cat";
    public static final int TIMEOUT = 1000;

    private WebClient getWebClient(String masterBaseUrl) {
        final var tcpClient = TcpClient
                .create()
                .option(ChannelOption.CONNECT_TIMEOUT_MILLIS, TIMEOUT)
                .doOnConnected(connection -> {
                    connection.addHandlerLast(new ReadTimeoutHandler(TIMEOUT, TimeUnit.MILLISECONDS));
                    connection.addHandlerLast(new WriteTimeoutHandler(TIMEOUT, TimeUnit.MILLISECONDS));
                });

        return WebClient.builder()
                .baseUrl(masterBaseUrl)
                .clientConnector(new ReactorClientHttpConnector(HttpClient.from(tcpClient)))
                .build();
    }

    @Bean(value = "WebCatClientConfiguration")
    public WebClient webCatClientConf() {
        return getWebClient(CAT_BASE_URL);
    }

    @Bean(value = "WebMasterClientConfiguration")
    public WebClient webMasterClientConf() {
        return getWebClient(MASTER_BASE_URL);
    }
}
