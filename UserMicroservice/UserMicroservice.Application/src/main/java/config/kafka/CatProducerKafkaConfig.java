package config.kafka;

import kafkaDto.CreateCatDto;
import kafkaDto.AddCatToMasterDto;
import kafkaDto.DeleteCatDto;
import kafkaDto.FriendshipDto;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CatProducerKafkaConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    private  Map<String, Object> getCatKafkaProperties() {
        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        properties.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        return properties;
    }

    @Bean
    public ProducerFactory<String, CreateCatDto> addCatDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getCatKafkaProperties());
    }

    @Bean
    public ProducerFactory<String, DeleteCatDto> deleteCatDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getCatKafkaProperties());
    }

    @Bean
    public ProducerFactory<String, FriendshipDto> catFriendshipDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getCatKafkaProperties());
    }

    @Bean
    public ProducerFactory<String, AddCatToMasterDto> addCatToMasterDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getCatKafkaProperties());
    }

    @Bean
    public KafkaTemplate<String, CreateCatDto> addCatDtoKafkaTemplate(ProducerFactory<String, CreateCatDto> addCatDtoProducerFactory) {
        return new KafkaTemplate<>(addCatDtoProducerFactory);
    }

    @Bean
    public KafkaTemplate<String, DeleteCatDto> deleteCatDtoKafkaTemplate(ProducerFactory<String, DeleteCatDto> deleteCatDtoProducerFactory) {
        return new KafkaTemplate<>(deleteCatDtoProducerFactory);
    }

    @Bean
    public KafkaTemplate<String, FriendshipDto> friendshipDtoKafkaTemplate(ProducerFactory<String, FriendshipDto> friendshipDtoProducerFactory) {
        return new KafkaTemplate<>(friendshipDtoProducerFactory);
    }

    @Bean
    public KafkaTemplate<String, AddCatToMasterDto> addCatToMasterDtoKafkaTemplate(ProducerFactory<String, AddCatToMasterDto> addCatToMasterDtoProducerFactory) {
        return new KafkaTemplate<>(addCatToMasterDtoProducerFactory);
    }
}
