package config.kafka;

import kafkaDto.*;
import org.apache.kafka.clients.producer.ProducerConfig;
import org.apache.kafka.common.serialization.StringSerializer;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.core.DefaultKafkaProducerFactory;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.kafka.core.ProducerFactory;
import org.springframework.kafka.support.serializer.JsonSerializer;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class MasterProducerKafkaConfig {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    private Map<String, Object> getMasterKafkaProperties() {
        Map<String, Object> properties = new HashMap<>();
        properties.put(ProducerConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        properties.put(ProducerConfig.KEY_SERIALIZER_CLASS_CONFIG, StringSerializer.class);
        properties.put(ProducerConfig.VALUE_SERIALIZER_CLASS_CONFIG, JsonSerializer.class);
        properties.put(JsonSerializer.ADD_TYPE_INFO_HEADERS, false);
        return properties;
    }

    @Bean
    public ProducerFactory<String, CreateMasterDto> addMasterDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getMasterKafkaProperties());
    }

    @Bean
    public ProducerFactory<String, DeleteMasterDto> deleteMasterDtoProducerFactory() {
        return new DefaultKafkaProducerFactory<>(getMasterKafkaProperties());
    }

    @Bean
    public KafkaTemplate<String, CreateMasterDto> addMasterDtoKafkaTemplate(ProducerFactory<String, CreateMasterDto> addMasterDtoProducerFactory) {
        return new KafkaTemplate<>(addMasterDtoProducerFactory);
    }

    @Bean
    public KafkaTemplate<String, DeleteMasterDto> deleteMasterDtoKafkaTemplate(ProducerFactory<String, DeleteMasterDto> deleteMasterDtoProducerFactory) {
        return new KafkaTemplate<>(deleteMasterDtoProducerFactory);
    }

}
