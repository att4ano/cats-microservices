package config.kafka;

import org.apache.kafka.clients.admin.AdminClientConfig;
import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;
import org.springframework.kafka.core.KafkaAdmin;

import java.util.HashMap;
import java.util.Map;

@Configuration
public class CatKafkaTopic {

    @Value("${spring.kafka.bootstrap-servers}")
    private String bootstrapServers;

    @Bean
    public KafkaAdmin kafkaAdmin() {
        Map<String, Object> configs = new HashMap<>();
        configs.put(AdminClientConfig.BOOTSTRAP_SERVERS_CONFIG, bootstrapServers);
        return new KafkaAdmin(configs);
    }

    @Bean
    public NewTopic createCatTopic() {
        return TopicBuilder.name("create_cat").partitions(1).replicas(1).build();
    }

    @Bean
    public NewTopic deleteCatTopic() {
        return TopicBuilder.name("delete_cat").partitions(1).replicas(1).build();
    }

    @Bean
    public NewTopic friendCatTopic() {
        return TopicBuilder.name("make_friendship").partitions(1).replicas(1).build();
    }

    @Bean
    public NewTopic unfriendCatTopic() {
        return TopicBuilder.name("end_friendship").partitions(1).replicas(1).build();
    }

    @Bean
    public NewTopic addMasterToCatTopic() {
        return TopicBuilder.name("add_master_to_cat").partitions(1).replicas(1).build();
    }
}
