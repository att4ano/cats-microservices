package config.kafka;

import org.apache.kafka.clients.admin.NewTopic;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.kafka.config.TopicBuilder;

@Configuration
public class MasterKafkaConfig {

    @Bean
    public NewTopic createMasterTopic() {
        return TopicBuilder.name("create_master").partitions(3).replicas(1).build();
    }

    @Bean
    public NewTopic deleteMasterTopic() {
        return TopicBuilder.name("delete_master").partitions(3).replicas(1).build();
    }
}
