package commons;

public class NotFoundException extends ApplicationException {

    public NotFoundException(String message) {
        super(message);
    }

    public static ApplicationException catNotFound() {
        return new NotFoundException("cat not found");
    }
}
