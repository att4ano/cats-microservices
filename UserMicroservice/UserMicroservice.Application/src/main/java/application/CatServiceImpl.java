package application;

import client.CatClient;
import commons.ApplicationException;
import commons.NotFoundException;
import dto.CatDto;
import dto.FilterDto;
import contracts.CatService;
import kafkaDto.CreateCatDto;
import kafkaDto.AddCatToMasterDto;
import kafkaDto.DeleteCatDto;
import kafkaDto.FriendshipDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ComponentScan(basePackages = "client")
public class CatServiceImpl implements CatService {

    private final CatClient catClient;
    private final KafkaTemplate<String, CreateCatDto> addCatDtoKafkaTemplate;
    private final KafkaTemplate<String, DeleteCatDto> deleteCatDtoKafkaTemplate;
    private final KafkaTemplate<String, FriendshipDto> friendshipDtoKafkaTemplate;
    private final KafkaTemplate<String, AddCatToMasterDto> addCatToMasterDtoKafkaTemplate;

    @Autowired
    public CatServiceImpl(CatClient catClient,
                          KafkaTemplate<String, CreateCatDto> addCatDtoKafkaTemplate,
                          KafkaTemplate<String, DeleteCatDto> deleteCatDtoKafkaTemplate,
                          KafkaTemplate<String, FriendshipDto> friendshipDtoKafkaTemplate,
                          KafkaTemplate<String, AddCatToMasterDto> addCatToMasterDtoKafkaTemplate) {
        this.catClient = catClient;
        this.addCatDtoKafkaTemplate = addCatDtoKafkaTemplate;
        this.deleteCatDtoKafkaTemplate = deleteCatDtoKafkaTemplate;
        this.friendshipDtoKafkaTemplate = friendshipDtoKafkaTemplate;
        this.addCatToMasterDtoKafkaTemplate = addCatToMasterDtoKafkaTemplate;
    }

    @Override
    public List<CatDto> checkAllCats(FilterDto filterDto) {
        return catClient.getAll(filterDto);
    }

    @Override
    public CatDto findCat(Long id) throws ApplicationException {

        CatDto catDto = catClient.getById(id);

        if (catDto == null) {
            throw NotFoundException.catNotFound();
        }

        return catClient.getById(id);
    }

    @Override
    public List<CatDto> checkAllFriendsOfCat(Long catId) {
        return catClient.getAllFriendsOfCat(catId);
    }

    @Override
    public void addCat(String name, String birthday, String breed, String color) {
        CreateCatDto createCatDto = new CreateCatDto(name, breed, color, birthday);
        addCatDtoKafkaTemplate.send("create_cat", createCatDto);
    }

    @Override
    public void deleteCat(Long id) {
        DeleteCatDto deleteCatDto = new DeleteCatDto(id);
        deleteCatDtoKafkaTemplate.send("delete_cat", deleteCatDto);
    }

    @Override
    public void makeFriends(Long firstCatId, Long secondCatId) {
        FriendshipDto friendshipDto = new FriendshipDto(firstCatId, secondCatId);
        friendshipDtoKafkaTemplate.send("make_friendship", friendshipDto);
    }

    @Override
    public void endFriendship(Long firstCatId, Long secondCatId) {
        FriendshipDto friendshipDto = new FriendshipDto(firstCatId, secondCatId);
        friendshipDtoKafkaTemplate.send("end_friendship", friendshipDto);
    }

    @Override
    public void addCatToMaster(Long catId, Long masterId) {
        AddCatToMasterDto addCatToMasterDto = new AddCatToMasterDto(catId, masterId);
        addCatToMasterDtoKafkaTemplate.send("add_master_to_cat", addCatToMasterDto);
    }
}
