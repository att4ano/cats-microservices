package application;

import client.CatClient;
import client.MasterClient;
import contracts.MasterService;
import dto.CatDto;
import dto.FilterDto;
import dto.MasterDto;
import kafkaDto.CreateMasterDto;
import kafkaDto.DeleteMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
@ComponentScan(basePackages = "client")
public class MasterServiceImpl implements MasterService {

    private final MasterClient masterClient;
    private final CatClient catClient;
    private final KafkaTemplate<String, CreateMasterDto> createMasterKafkatemplate;
    private final KafkaTemplate<String, DeleteMasterDto> deleteMasterKafkatemplate;

    @Autowired
    public MasterServiceImpl(MasterClient masterClient,
                             CatClient catClient,
                             KafkaTemplate<String, CreateMasterDto> createMasterKafkatemplate,
                             KafkaTemplate<String, DeleteMasterDto> deleteMasterKafkatemplate) {
        this.masterClient = masterClient;
        this.catClient = catClient;
        this.createMasterKafkatemplate = createMasterKafkatemplate;
        this.deleteMasterKafkatemplate = deleteMasterKafkatemplate;
    }

    @Override
    public List<MasterDto> checkAllMasters() {
        return masterClient.getAll();
    }

    @Override
    public MasterDto findMasterById(Long id) {
        return masterClient.getById(id);
    }

    @Override
    public MasterDto findMasterByName(String name) {
        return masterClient.getByName(name);
    }

    @Override
    public List<CatDto> checkAllCatsOfMaster(Long masterId) {

        return catClient.getAll(new FilterDto(null, null, null))
                .stream()
                .filter(catDto -> catDto.masterId().equals(masterId))
                .toList();
    }

    @Override
    public void addMaster(String name, String birthday) {
        CreateMasterDto createMasterDto = new CreateMasterDto(name, birthday);
        createMasterKafkatemplate.send("create_master", createMasterDto);
    }

    @Override
    public void deleteMaster(Long id) {
        DeleteMasterDto deleteMasterDto = new DeleteMasterDto(id);
        deleteMasterKafkatemplate.send("delete_master", deleteMasterDto);
    }
}
