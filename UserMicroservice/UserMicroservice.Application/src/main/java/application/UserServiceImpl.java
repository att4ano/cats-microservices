package application;

import contracts.UserService;
import mapper.UserMapper;
import models.User;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repositories.UserRepository;

import java.util.List;

@Service
@Transactional
@ComponentScan(basePackages = {"repositories", "config"})
public class UserServiceImpl implements UserService {

    private final UserRepository userRepository;
    private final PasswordEncoder passwordEncoder;

    @Autowired
    public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder) {
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
    }

    @Override
    public List<User> getAll() {
        return userRepository.findAll()
                .stream().map(UserMapper::entityToModel)
                .toList();
    }

    @Override
    public User getUserById(Long id) {
        return UserMapper.entityToModel(userRepository.findUserEntityById(id));
    }

    @Override
    public void addUser(String username, String password, String role) {
        userRepository.add(username, passwordEncoder.encode(password), role);
    }

    @Override
    public void deleteUser(Long id) {
        userRepository.deleteUserEntityById(id);
    }
}
