package client;

import dto.MasterDto;

import java.util.List;

public interface MasterClient {

    List<MasterDto> getAll();

    MasterDto getById(Long id);

    MasterDto getByName(String name);
}
