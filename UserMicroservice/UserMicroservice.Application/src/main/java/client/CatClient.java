package client;

import dto.CatDto;
import dto.FilterDto;

import java.util.List;

public interface CatClient {

    List<CatDto> getAll(FilterDto filterDto);

    CatDto getById(Long id);

    List<CatDto> getAllFriendsOfCat(Long id);
}
