package client;

import dto.CatDto;
import dto.FilterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.http.HttpMethod;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Component;
import org.springframework.web.client.RestTemplate;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Optional;

@Component
@ComponentScan(basePackages = "config.web")
public class CatClientImpl implements CatClient {

    private final WebClient webClient;

    @Autowired
    public CatClientImpl(@Qualifier("WebCatClientConfiguration") WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public List<CatDto> getAll(FilterDto filterDto) {
        UriComponentsBuilder uriBuilder = UriComponentsBuilder
                .fromPath("/");

        if (filterDto.id() != null) {
            uriBuilder.queryParam("id", filterDto.id());
        }

        if (filterDto.color() != null) {
            uriBuilder.queryParam("color", filterDto.color());
        }

        if (filterDto.breed() != null) {
            uriBuilder.queryParam("breed", filterDto.breed());
        }

        String url = uriBuilder.toUriString();

        Mono<List<CatDto>> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });

        return response.block();
    }

    @Override
    public CatDto getById(Long id) {
        String url = UriComponentsBuilder
                .fromPath("/{id}")
                .buildAndExpand(id)
                .toUriString();

        Mono<CatDto> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(CatDto.class);

        return response.block();
    }

    @Override
    public List<CatDto> getAllFriendsOfCat(Long id) {
        String url = UriComponentsBuilder
                .fromPath("/friends/{id}")
                .buildAndExpand(id)
                .toUriString();

        Mono<List<CatDto>> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });

        return response.block();
    }
}
