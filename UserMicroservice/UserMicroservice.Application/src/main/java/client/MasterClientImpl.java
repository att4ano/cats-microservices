package client;

import dto.MasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.core.ParameterizedTypeReference;
import org.springframework.stereotype.Component;
import org.springframework.web.reactive.function.client.WebClient;
import org.springframework.web.util.UriComponentsBuilder;
import reactor.core.publisher.Mono;

import java.util.List;

@Component
@ComponentScan("config.web")
public class MasterClientImpl implements MasterClient {

    private final WebClient webClient;

    @Autowired
    public MasterClientImpl(@Qualifier("WebMasterClientConfiguration") WebClient webClient) {
        this.webClient = webClient;
    }

    @Override
    public List<MasterDto> getAll() {
        String url = UriComponentsBuilder
                .fromPath("/")
                .toUriString();

        Mono<List<MasterDto>> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(new ParameterizedTypeReference<>() {
                });

        return response.block();
    }

    @Override
    public MasterDto getById(Long id) {
        String url = UriComponentsBuilder
                .fromPath("/{id}")
                .buildAndExpand(id)
                .toUriString();

        Mono<MasterDto> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(MasterDto.class);

        return response.block();
    }

    @Override
    public MasterDto getByName(String name) {
        String url = UriComponentsBuilder
                .fromPath("/name/{name}")
                .buildAndExpand(name)
                .toUriString();

        Mono<MasterDto> response = webClient.get()
                .uri(url)
                .retrieve()
                .bodyToMono(MasterDto.class);

        return response.block();
    }
}
