package dto;

import lombok.Builder;

@Builder
public record FilterDto(String color, Long id, String breed) { }
