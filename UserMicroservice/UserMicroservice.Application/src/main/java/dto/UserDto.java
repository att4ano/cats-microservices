package dto;

import lombok.Builder;

@Builder
public record UserDto(Long id, String name, String password, String role, Long masterId) { }
