package mapper;

import dto.UserDto;
import entities.UserEntity;
import lombok.experimental.UtilityClass;
import models.User;

@UtilityClass
public final class UserMapper {

    public static UserDto modelToDto(User userModel) {

        if (userModel == null) {
            return null;
        }

        return UserDto.builder()
                .id(userModel.getId())
                .name(userModel.getUsername())
                .password(userModel.getPassword())
                .role(userModel.getRole())
                .masterId(userModel.getId())
                .build();
    }

    public static User entityToModel(UserEntity userEntity) {

        if (userEntity == null) {
            return null;
        }

        return User.builder()
                .id(userEntity.getId())
                .username(userEntity.getUsername())
                .password(userEntity.getPassword())
                .role(userEntity.getRole())
                .masterId(userEntity.getMasterId())
                .build();
    }
}
