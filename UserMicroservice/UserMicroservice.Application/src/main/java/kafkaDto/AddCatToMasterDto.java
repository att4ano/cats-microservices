package kafkaDto;

public record AddCatToMasterDto(Long catId, Long masterId) {
}
