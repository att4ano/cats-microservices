package kafkaDto;

public record CreateMasterDto(String name, String birthdayDate) { }
