package kafkaDto;

public record CreateCatDto(String name, String breed, String color, String birthdayDate) { }
