package kafkaDto;

public record DeleteMasterDto(Long id) {
}
