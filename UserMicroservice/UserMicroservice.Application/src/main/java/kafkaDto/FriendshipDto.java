package kafkaDto;

public record FriendshipDto(Long firstId, Long secondId) { }
