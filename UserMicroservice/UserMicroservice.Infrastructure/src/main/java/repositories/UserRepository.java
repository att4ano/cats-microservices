package repositories;

import entities.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<UserEntity, Long> {

    UserEntity findUserEntityByUsername(String username);

    UserEntity findUserEntityById(Long id);

    @Modifying
    @Query(value = "INSERT INTO users (name, password, role) VALUES (?, ?, ?)",
            nativeQuery = true)
    void add(@Param("name") String name, @Param("password") String password, @Param("role") String role);

    void deleteUserEntityById(Long id);
}