package contracts;

import models.Master;

import java.util.List;

public interface MasterService {

    List<Master> checkAllMasters();

    Master findMasterById(Long id);

    Master findMasterByName(String name);

    void addMaster(String name, String birthday);

    void deleteMaster(Long id);
}