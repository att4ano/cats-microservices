package mappers;

import dto.MasterDto;
import entities.MasterEntity;
import lombok.experimental.UtilityClass;
import models.Master;

@UtilityClass
public final class MasterMapper {

    public static MasterDto modelToDto(Master masterModel) {

        if (masterModel == null) {
            return null;
        }

        MasterDto masterDto = MasterDto.builder()
                .id(masterModel.getId())
                .name(masterModel.getName())
                .birthdayDate(masterModel.getBirthdayDate().toString())
                .build();

        return masterDto;
    }

    public static Master entityToModel(MasterEntity masterEntity) {

        if (masterEntity == null) {
            return null;
        }

        Master masterModel = Master.builder()
                .id(masterEntity.getId())
                .name(masterEntity.getName())
                .birthdayDate(masterEntity.getBirthdayDate())
                .build();

        return masterModel;
    }
}