package repositories;

import entities.MasterEntity;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;

@Repository
public interface MasterRepository extends JpaRepository<MasterEntity, Long> {

    MasterEntity findMasterEntityById (Long id);

    MasterEntity findMasterEntityByName(String name);

    @Modifying
    @Query(value = "INSERT INTO public.masters (name, birthday_date) VALUES (?, ?)",
            nativeQuery = true)
    void add(@Param("name") String name, @Param("birthday_date") LocalDate birthday_date);

    void deleteMasterEntityById(Long id);
}