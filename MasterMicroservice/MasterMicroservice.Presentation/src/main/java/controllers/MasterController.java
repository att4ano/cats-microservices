package controllers;

import contracts.MasterService;
import dto.MasterDto;
import mappers.MasterMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("/master")
@ComponentScan(basePackages = "application")
public class MasterController {
    private final MasterService masterService;

    @Autowired
    public MasterController(MasterService masterService) {
        this.masterService = masterService;
    }

    @GetMapping("/")
    public ResponseEntity<List<MasterDto>> getAllMasters() {
        return ResponseEntity.ok(masterService.checkAllMasters()
                .stream()
                .map(MasterMapper::modelToDto)
                .toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<MasterDto> getMasterById(@PathVariable("id") long id) {
        return ResponseEntity.ok(MasterMapper.modelToDto(masterService.findMasterById(id)));
    }

    @GetMapping("/name/{name}")
    public ResponseEntity<MasterDto> getMasterByName(@PathVariable("name") String name) {
        return ResponseEntity.ok(MasterMapper.modelToDto(masterService.findMasterByName(name)));
    }
}
