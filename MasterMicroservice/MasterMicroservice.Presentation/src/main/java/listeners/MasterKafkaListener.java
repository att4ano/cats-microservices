package listeners;

import contracts.MasterService;
import kafkaDto.CreateMasterDto;
import kafkaDto.DeleteMasterDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackages = {"application", "config"})
public class MasterKafkaListener {

    private final MasterService masterService;

    @Autowired
    public MasterKafkaListener(MasterService masterService) {
        this.masterService = masterService;
    }

    @KafkaListener(topics = "create_master",
            groupId = "createMasterGroup",
            containerFactory = "createMasterDtoConcurrentKafkaListenerContainerFactory")
    public void addMaster(CreateMasterDto masterDto) {
        masterService.addMaster(masterDto.name(), masterDto.birthdayDate());
    }

    @KafkaListener(topics = "delete_master",
            groupId = "deleteMasterGroup",
            containerFactory = "deleteMasterDtoConcurrentKafkaListenerContainerFactory")
    public void deleteMaster(DeleteMasterDto deleteMasterDto) {
        masterService.deleteMaster(deleteMasterDto.id());
    }
}
