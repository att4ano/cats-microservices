package repositories;

import entities.CatEntity;
import lombok.NonNull;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import java.time.LocalDate;
import java.util.List;

@Repository
public interface CatRepository extends JpaRepository<CatEntity, Long> {

    @NonNull
    List<CatEntity> findAll();

    CatEntity findCatEntityById(Long id);

    void deleteCatEntitiesById(Long id);

    @Query(value = "SELECT cats.id, cats.name, cats.birthday_date, cats.breed, cats.color, cats.master_id\n" +
            "FROM friends\n" +
            "         JOIN cats ON friends.first_friend_id = cats.id\n" +
            "WHERE friends.second_friend_id = :id", nativeQuery = true)
    List<CatEntity> findAllFriends(@Param("id") Long id);

    @Modifying
    @Query(value = "INSERT INTO friends (first_friend_id, second_friend_id) VALUES (:firstFriendId, :secondFriendId), (:secondFriendId, :firstFriendId)",
            nativeQuery = true)
    void makeFriends(@Param("firstFriendId") Long firstFriendId, @Param("secondFriendId") Long secondFriendId);

    @Modifying
    @Query(value = "DELETE FROM friends WHERE (first_friend_id = :firstFriendId AND second_friend_id = :secondFriendId) OR (first_friend_id = :secondFriendId AND second_friend_id = :firstFriendId)",
            nativeQuery = true)
    void endFriendship(@Param("firstFriendId") Long firstFriendId, @Param("secondFriendId") Long secondFriendId);

    @Modifying
    @Query(value = "INSERT INTO cats (name, birthday_date, breed, color) VALUES (:name, :birthday_date, :breed, :color)",
            nativeQuery = true)
    void addNewEntity(@Param("name") String name, @Param("birthday_date") LocalDate birthday_date, @Param("breed") String breed, @Param("color") String color);

    @Modifying
    @Query(value = "UPDATE cats SET master_id = :masterId WHERE id = :catId", nativeQuery = true)
    void addCatToMaster(@Param("catId") Long catId, @Param("masterId") Long masterId);

}