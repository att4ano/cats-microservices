package listeners;

import contracts.CatService;
import kafkaDto.AddCatToMasterDto;
import kafkaDto.CreateCatDto;
import kafkaDto.DeleteCatDto;
import kafkaDto.FriendshipDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Component;

@Component
@ComponentScan(basePackages = {"application", "config"})
public class CatKafkaListener {
    private final CatService catService;

    @Autowired
    public CatKafkaListener(CatService catService) {
        this.catService = catService;
    }

    @KafkaListener(topics = "create_cat",
            groupId = "createCatGroup",
            containerFactory = "createCatDtoConcurrentKafkaListenerContainerFactory")
    public void addCat(CreateCatDto catDto) {
        System.out.println(catDto.name() + " " + catDto.birthdayDate() + " " + catDto.breed() + " " + catDto.color());
        catService.addCat(catDto.name(), catDto.birthdayDate(), catDto.breed(), catDto.color());
    }

    @KafkaListener(topics = "delete_cat",
            groupId = "deleteCatGroup",
            containerFactory = "deleteCatDtoConcurrentKafkaListenerContainerFactory")
    public void deleteCat(DeleteCatDto deleteCatDto) {
        catService.deleteCat(deleteCatDto.id());
    }

    @KafkaListener(topics = "make_friendship",
            groupId = "friendshipGroup",
            containerFactory = "friendshipDtoConcurrentKafkaListenerContainerFactory")
    public void makeFriends(FriendshipDto friendshipDto) {
        System.out.println(friendshipDto.firstId() + " " + " " + friendshipDto.secondId());
        catService.makeFriends(friendshipDto.firstId(), friendshipDto.secondId());
    }

    @KafkaListener(topics = "end_friendship",
            groupId = "friendshipGroup",
            containerFactory = "friendshipDtoConcurrentKafkaListenerContainerFactory")
    public void endFriendShip(FriendshipDto friendshipDto) {
        catService.endFriendship(friendshipDto.firstId(), friendshipDto.secondId());
    }

    @KafkaListener(topics = "add_master_to_cat",
            groupId = "addCatToMasterGroup",
            containerFactory = "addCatToMasterDtoConcurrentKafkaListenerContainerFactory")
    public void addCatToMaster(AddCatToMasterDto addCatToMasterDto) {
        catService.addCatToMaster(addCatToMasterDto.catId(), addCatToMasterDto.masterId());
    }
}
