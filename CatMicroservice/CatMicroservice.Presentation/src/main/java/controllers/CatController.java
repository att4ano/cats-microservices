package controllers;

import commons.ApplicationException;
import contracts.CatService;
import dto.CatDto;
import dto.FilterDto;
import lombok.RequiredArgsConstructor;
import mappers.CatMapper;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/cat")
@RequiredArgsConstructor
@ComponentScan(basePackages = {"application"})
public class CatController {

    private final CatService catService;

    @GetMapping("/")
    public ResponseEntity<List<CatDto>> getAllCats(@RequestParam(value = "color", required = false) String color,
                                                   @RequestParam(value = "breed", required = false) String breed,
                                                   @RequestParam(value = "masterId", required = false) Long masterId) {
        return ResponseEntity.ok(
                catService.checkAllCats(new FilterDto(color, masterId, breed))
                        .stream()
                        .map(CatMapper::modelToDto)
                        .toList());
    }

    @GetMapping("/{id}")
    public ResponseEntity<CatDto> getCatById(@PathVariable("id") Long id) throws ApplicationException {
        return ResponseEntity.ok(CatMapper.modelToDto(catService.findCat(id)));
    }

    @GetMapping("/friends/{id}")
    public ResponseEntity<List<CatDto>> getAllFriendsOfCat(@PathVariable("id") Long id) {
        return ResponseEntity.ok(catService.checkAllFriendsOfCat(id)
                .stream()
                .map(CatMapper::modelToDto)
                .toList());
    }

}