package dto;

public record FilterDto(String color, Long id, String breed) { }
