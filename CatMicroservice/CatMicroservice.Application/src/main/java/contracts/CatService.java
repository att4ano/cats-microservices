package contracts;

import commons.ApplicationException;
import commons.NotFoundException;
import dto.FilterDto;
import models.Cat;

import java.util.List;

public interface CatService {

    List<Cat> checkAllCats(FilterDto filterDto);

    List<Cat> checkAllFriendsOfCat(Long catId);

    Cat findCat(Long id) throws ApplicationException;

    void addCat(String name, String birthday, String breed, String color);

    void deleteCat(Long id);

    void makeFriends(Long firstCatId, Long secondCatId);

    void endFriendship(Long firstCatId, Long secondCatId);

    void addCatToMaster(Long catId, Long masterId);
}