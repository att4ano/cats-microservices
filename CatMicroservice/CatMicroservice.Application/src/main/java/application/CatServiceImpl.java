package application;

import commons.ApplicationException;
import commons.NotFoundException;
import contracts.CatService;
import dto.FilterDto;
import filters.Filter;
import entities.CatEntity;
import mappers.CatMapper;
import mappers.FilterMapper;
import models.Cat;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import repositories.CatRepository;

import java.time.LocalDate;
import java.util.List;

@Service
@Transactional
@ComponentScan(basePackages = {"repositories"})
public class CatServiceImpl implements CatService {

    private final CatRepository catRepository;

    @Autowired
    public CatServiceImpl(CatRepository catRepository) {
        this.catRepository = catRepository;
    }

    @Override
    public List<Cat> checkAllCats(FilterDto filterDto) {

        List<CatEntity> cats = catRepository.findAll();

        Filter filterModel = FilterMapper.dtoToFilter(filterDto);

        return cats.stream()
                .map(CatMapper::entityToModel)
                .filter(filterModel::filter)
                .toList();
    }

    @Override
    public List<Cat> checkAllFriendsOfCat(Long catId) {
        return catRepository
                .findAllFriends(catId)
                .stream()
                .map(CatMapper::entityToModel)
                .toList();
    }

    @Override
    public Cat findCat(Long id) throws ApplicationException {

        CatEntity catEntity = catRepository.findCatEntityById(id);

        if (catEntity == null)
            throw NotFoundException.catNotFound();

        return CatMapper.entityToModel(catRepository.findCatEntityById(id));
    }

    @Override
    public void addCat(String name, String birthday, String breed, String color) {
        System.out.println(birthday);
        catRepository.addNewEntity(name, LocalDate.parse(birthday), breed, color);
    }

    @Override
    public void deleteCat(Long id) {
        catRepository.deleteCatEntitiesById(id);
    }

    @Override
    public void makeFriends(Long firstCatId, Long secondCatId) {
        catRepository.makeFriends(firstCatId, secondCatId);
    }

    @Override
    public void endFriendship(Long firstCatId, Long secondCatId) {
        catRepository.endFriendship(firstCatId, secondCatId);
    }

    @Override
    public void addCatToMaster(Long catId, Long masterId) {
        catRepository.addCatToMaster(catId, masterId);
    }
}