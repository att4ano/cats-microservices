package mappers;

import lombok.experimental.UtilityClass;
import models.Cat;
import dto.CatDto;
import entities.CatEntity;

import java.util.ArrayList;

@UtilityClass
public final class CatMapper {
    public static CatDto modelToDto(Cat catModel) {
        CatDto catDto = CatDto.builder()
                .id(catModel.getId())
                .name(catModel.getName())
                .breed(catModel.getBreed())
                .color(catModel.getColor())
                .birthdayDate(catModel.getBirthdayDate().toString())
                .masterId(catModel.getMasterId())
                .friends(new ArrayList<>())
                .build();

        for (var cat : catModel.getCatFriends()) {
            CatDto friendDto = CatDto.builder()
                    .id(cat.getId())
                    .name(cat.getName())
                    .color(cat.getBreed())
                    .breed(cat.getBreed())
                    .birthdayDate(cat.getBirthdayDate().toString())
                    .masterId(cat.getMasterId())
                    .friends(new ArrayList<>())
                    .build();

            catDto.friends().add(friendDto);
        }

        return catDto;
    }

    public static Cat entityToModel(CatEntity catEntity) {

        return Cat.builder()
                .id(catEntity.getId())
                .name(catEntity.getName())
                .breed(catEntity.getBreed())
                .color(catEntity.getColor())
                .birthdayDate(catEntity.getBirthdayDate())
                .masterId(catEntity.getMasterId())
                .catFriends(new ArrayList<>())
                .build();
    }
}