package mappers;

import dto.FilterDto;
import filters.Filter;
import lombok.experimental.UtilityClass;
import models.Cat;

@UtilityClass
public final class FilterMapper {

    public static Filter dtoToFilter(FilterDto filterDto) {
        Filter filterModel = (Cat cat) -> true;

        if (filterDto.id() != null) {
            Filter tmpFilterModel = filterModel;
            filterModel = (Cat cat) -> tmpFilterModel.filter(cat) && cat.getMasterId().equals(filterDto.id());
        }

        if (filterDto.color() != null) {
            Filter tmpFilterModel = filterModel;
            filterModel = (Cat cat) -> tmpFilterModel.filter(cat) && cat.getColor().equals(filterDto.color());
        }

        if (filterDto.breed() != null) {
            Filter tmpFilterModel = filterModel;
            filterModel = (Cat cat) -> tmpFilterModel.filter(cat) && cat.getBreed().equals(filterDto.breed());
        }

        return filterModel;
    }
}
