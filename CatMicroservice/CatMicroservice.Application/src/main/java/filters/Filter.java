package filters;

import models.Cat;

public interface Filter {
    Boolean filter(Cat cat);
}
